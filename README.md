# Li--le link shortener

## Project purpose
Li--le (pronounce as little) is a small Kotlin project that show huge capabilities 
(but of course not all) of Kotlin language, and some existing frameworks
for web development as well as the configuration process of Gradle application
which can be automatically deployed to webserver.

## Project parts

### Application server

Server is handled with the **ktor** framework that allows us to define the dependency on embeddable
server that runs our application (in this case Netty engine is used).

Ktor allows for simple server management as well as extra modules instalation. The automatic forwarding to HTTPS
plugin is installed in this project alongsite custom paths requests mappings (provided  with *Locations* plugin)

### Database access layer

The access to database is provided via [Exposed](https://github.com/JetBrains/Exposed) JetBrains library
that allows defining simple tables in kotlin code with just a few lines. Table definition as a Kotlin 
object is needed in order to specify the the table contraints. It can be a source of table (tables can 
be created using `SchemaUtils.createMissingTablesAndColumns` function as in this example) but generally
the migrations process has to be independent from data access in this framework.

Exposed offers two levels of database access: typesafe SQL wrapping DSL and lightweight data access objects.
As in this project only few queries to database are used so writing typesafe SQL in Kotlin gives more fun
for me as a developer.

There is also a special function worth noticing 
```kotlin
suspend fun <T> dbQuery(statement: Transaction.() -> T): T =
    withContext(Dispatchers.IO) {
        transaction(DbSettings.connection, statement)
    }
```
which is used in almost all other functions as it defines query context to make it  executed as a suspending
coroutine (read more in **Utility functions**)


### Development running

#### Developer environment

It is configured to make it as simple as possible for developer to work with this application locally so  
the PostgreSQL docker image is used as a database backed (with standard configuration as run only locally).

The scripts starting and stopping the database docker image with shared port are available in [scripts](./scripts)
directory, and they are called as similarly named gradle tasks. Gradle allows to define the dependencies between
different tasks so there is a constraint to run *stopDockerDatabase* task before *startDockerDatabase* and
to force running *startDockerDatabase* before *run* tasks (which is *application* plugin task to run application
and is really useful in development of the application).

### Production environment

When app is deployed to server it is built using Heroku default Gradle task named *stage* and configured with
system variables that overwrite the default (developer values). In order to make it configured in this way
in **ktor**, special syntax is used in [application.conf](./resources/application.conf) file that allows us 
reading and overwriting the configuration variable when some other value is defined.

### Utility functions

There are just a few util functions defined in project, but the main reason of writing them is to show
the big flexibility of Kotlin language that is achieved in different ways.

#### Util.kt
```kotlin
private val AVAILABLE_CHARS = ('a'..'z') + ('A'..'Z') + ('0'..'9')
```
Kotlin allows to define the operators for specific types so there is a beautifully
used range operator (double dot) that crates range object (in this case _CharRange_).
Everything is done via implicit call of standard library function _rangeTo_. Additionally,
we have the ability to define the file variables that are private for file scope, so they can
be used with no worry about global visibility.
```kotlin
fun randomString(length: Int = MODEL_URL_LENGTH) = List(length) { AVAILABLE_CHARS.random() }.joinToString("")
```
Kotlin also gives user the power of default values in functions (also constructors), so we can
easily define the function that can be more abstract but for now has a specific usage without lost
of abstraction.
Moreover, this sample shows how simply can be defined **function** that has a simple body, so
it can be placed in one line - Kotlin allows defining functions using equality character.
We can also see how tricky can be the oneliner for random string generation - the Kotlin lambda with braces { } is
used to generate the elements of characters list from specified range (as every range in Kotlin has
random function - that really makes sense) and then the elements of list are joined using standard library function.

```kotlin
inline fun onProd(onProdAction: () -> Unit) = if (!IS_DEV) onProdAction() else Unit
```
The last function in this file shows only partially the abilities of Kotlin inline functions, but
it specifies simple syntax to get code run only during production run of application. The simple usage of
```kotlin
onProd {
    onlyProductionCalledFunction()
}
```
shows not only the power of creating DSLs in Kotlin but also reminds the developer that
Kotlin covers many aspects of functional language as usually every statement in thi language returns 
some value - so in this case the specific _Unit_ value is returned on developer environment instead
of calling some functions.

#### Database.kt
```kotlin
suspend fun <T> dbQuery(statement: Transaction.() -> T): T =
    withContext(Dispatchers.IO) {
        transaction(DbSettings.connection, statement)
    }
```
This quite short sample covers many parts of Kotlin language as it covers:
1. Kotlin coroutines marked as _suspend_ functions that can be executed as they would
be lightweight threads, but they can return some values which are used in some other coroutine 
like they would have been sequentially called callbacks on some promise-returing functions.
As the framework _transaction_ function is blocking one it needs to be executed
in the context of thread other the main one (thread pool of IO threads which should be used for
IO operations like network/database connections) in order to make this function suspendable.
2. Kotlin function interfaces defined with special syntax ```Receiver.() -> ReturnType```
which allows building DSLs with the specified _Receiver_ object. Additionally, as the _statement_
is the last argument of defined function it (as the labda parameter) can be placed outside
the function arguments list in braces like
```kotlin
dbQuery<T> {
    someQueryFunctionReturingT()
}
```
where ```<T>``` can be missed then can be deduced by a compiler (so almost in any case).
 3. Kotlin allows for lazy evaluation of before getting some variable, so the variable can get created when 
it is used for the first time, as in this example ```DbSettings.connection``` is initialized once when it is
used for the first time. The best part of this tricky approach is that the type of ```DbSettings.connection```
is it wouldn't have been defined as _Lazy_ value.

### Auto deployment configuration

Project is deployed to [Heroku](https://www.heroku.com) as a free account single dyno application
with access to limited PostgreSQL database storage. The application has to be created manually
using website but after first manual push to heroku git there is simple approach to
automate the process of deployment using Github Actions or Gitlab CI (as in this example).

The production process defined in [.gitlab-ci.yml](./.gitlab-ci.yml) checkouts the latest code from the repository
on separated environment (on Gitlab server) and in the separate folder clones the repository that 
is located on Heroku server from which the final application is built. Then Heroku repository is cleared, and the latest
files from Gitlab are pushed to heroku. Connection to Heroku is secured via HEROKU_API_KEY defined as environment variable
which allows to expose the Gitlab CI configuration file.

When the code is pushed to Heroku, the build process on Heroku start because server detects Gradle project
pushed. There has to be defined **stage** task in Gradle that builds the application as the single jar file (so it is 
enough to make this task depends on **clean** and **shadowJar** tasks).
Then the application is deployed as there is Heroku configuration file ([Procfile](./Procfile)) which contains the 
path to the build result.

In order properly run the application there must be defined some environment variables that configures the 
application run (but have some default values in configuration file, so it is possible to deploy app locally without
setting any environment variables)

| ENV VARIABLE NAME |                         VALUE                        |
|-------------------|------------------------------------------------------|
| PORT              | auto defined by Heroku as app is behind proxy server |
| ENV               | prod (another than 'dev' is enough)                  |
| JDBC_DATABASE_URL | auto defined by Heroku after PostgreSQL installation |

Using this configuration application can run using only https protocol with access to production database without any 
problems.