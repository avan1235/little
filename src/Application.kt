package com.procyk.maciej

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.netty.*

fun main(args: Array<String>): Unit = EngineMain.main(args)

fun Application.module() {

    blockingCreateTables()
    install(Locations)
    install(StatusPages) {
        status(HttpStatusCode.NotFound) {
            call.respondRedirect(HOME_PATH)
        }
    }

    onProd {
        install(XForwardedHeaderSupport)
        install(HttpsRedirect) {
            sslPort = 443
            permanentRedirect = true
        }
    }

    routing {

        get(HOME_PATH) {
            call.respondHtml { getMainPage() }
        }

        get(CSS_PATH) {
            call.respondCss { getCss() }
        }

        post(CREATE_MAPPING_PATH) {
            val mappingInput = call.receiveParameters()
            mappingInput[MAPPING_PARAM_ID]?.let {
                val shortened = getShortenedLink(call, it)
                call.respondHtml { getShortenedLinkPage(shortened) }
            } ?: call.respondRedirect(HOME_PATH, permanent = true)
        }

        @Location("/{shortenId}")
        class MappedLocation(val shortenId: String)
        get<MappedLocation> {
            val toUrl = getMappedLocation(it.shortenId)
            call.respondRedirect(toUrl ?: HOME_PATH, permanent = true)
        }
    }
}
