package com.procyk.maciej

private val AVAILABLE_CHARS = ('a'..'z') + ('A'..'Z') + ('0'..'9')

fun randomString(length: Int = MODEL_URL_LENGTH) = List(length) { AVAILABLE_CHARS.random() }.joinToString("")

inline fun onProd(onProdAction: () -> Unit) = if (!IS_DEV) onProdAction() else Unit
