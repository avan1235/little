package com.procyk.maciej

import kotlinx.html.*

fun HTML.getMainPage() {
    styledWithCss()
    body {
        div(classes = "h40 header") { +"Make your link little" }
        div(classes = "h60 top") {
            form(
                    method = FormMethod.post,
                    action = CREATE_MAPPING_PATH,
                    encType = FormEncType.applicationXWwwFormUrlEncoded,
                    classes = "h100 w80"
            ) {
                input(name = MAPPING_PARAM_ID, classes = "h100, w80")
                button(type = ButtonType.submit, classes = "h100") {
                    +"Shorten"
                }
            }
        }
    }
}

fun HTML.getShortenedLinkPage(shortened: String) {
    styledWithCss()
    body {
        div(classes = "h30 header") { +"Your little link" }
        div(classes = "h40") { a(shortened) { +shortened } }
        div(classes = "h20 bottom padB10") { a(HOME_PATH) { +"Main page" } }
    }
}