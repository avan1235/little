package com.procyk.maciej

import com.typesafe.config.ConfigFactory
import io.ktor.config.*

private val config = HoconApplicationConfig(ConfigFactory.load())

val ENVIRONMENT get() = config.property("ktor.deployment.environment").getString()

val IS_DEV get() = ENVIRONMENT == "dev"

val JDBC_DATABASE_URL get() = config.property("ktor.database.url").getString()

val MODEL_URL_LENGTH get() = config.property("ktor.model.url.length").getString().toInt()

const val MAPPING_PARAM_ID = "from"

const val HOME_PATH = "/"

const val CSS_PATH = "/static/styles.css"

const val CREATE_MAPPING_PATH = "/url-mapping/create"