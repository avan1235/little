package com.procyk.maciej

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import kotlinx.css.*
import kotlinx.css.properties.LineHeight
import kotlinx.css.properties.TextDecoration
import kotlinx.html.HTML
import kotlinx.html.head
import kotlinx.html.link

fun HTML.styledWithCss() = head {
    link(rel = "stylesheet", href = CSS_PATH)
}

suspend inline fun ApplicationCall.respondCss(builder: CSSBuilder.() -> Unit) {
    respondText(CSSBuilder().apply(builder).toString(), ContentType.Text.CSS)
}

fun CSSBuilder.getCss() {
    fun CSSBuilder.full() {
        margin(0.pct)
        padding(0.pct)
        width = 100.pct
        height = 100.pct
    }
    html { full() }
    body { full() }
    div {
        width = 100.pct
        height = 50.pct
        display = Display.flex
        justifyContent = JustifyContent.center
        alignItems = Align.center
        fontSize = 32.px
        lineHeight = LineHeight("1")
    }
    button {
        borderBottomRightRadius = 16.px
        borderTopRightRadius = 16.px
        borderWidth = 0.pct
        fontSize = 32.px
        lineHeight = LineHeight("1")
        color = Color.white
        backgroundColor = Color.gray
        cursor = Cursor.pointer
        width = LinearDimension.available
        padding(24.px)
    }
    input {
        fontSize = 32.px
        lineHeight = LineHeight("1")
        height = 74.px
        paddingLeft = 16.px
        paddingRight = 16.px
        borderWidth = 2.px
        borderBottomLeftRadius = 4.px
        borderTopLeftRadius = 4.px
        borderStyle = BorderStyle.solid
        borderColor = Color.gray
    }
    a {
        color = Color.darkGray
        textDecoration = TextDecoration.none
    }
    repeat(10) {
        val percentage = it * 10
        rule(".h$percentage") {
            height = percentage.pct
        }
        rule(".w$percentage") {
            width = percentage.pct
        }
    }
    repeat(20) {
        val percentage = it * 5
        rule(".padB$percentage") {
            paddingBottom = percentage.vh
        }
        rule(".padT$percentage") {
            paddingTop = percentage.vh
        }
        rule(".padL$percentage") {
            paddingLeft = percentage.vw
        }
        rule(".padR$percentage") {
            paddingRight = percentage.vw
        }
    }
    rule(".top") {
        alignItems = Align.flexStart
    }
    rule(".bottom") {
        alignItems = Align.flexEnd
    }
    rule(".sansserif") {
        fontFamily = "Arial, Helvetica, sans-serif"
    }
    rule(".header") {
        fontSize = 4.em
        lineHeight = LineHeight("1")
        fontStyle = FontStyle.normal
        fontWeight = FontWeight.bold
        color = Color.darkGray
    }
}