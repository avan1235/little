package com.procyk.maciej

import io.ktor.application.*
import io.ktor.request.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

fun blockingCreateTables() {
    transaction(DbSettings.connection) {
        SchemaUtils.createMissingTablesAndColumns(UrlMappingTable)
    }
}

suspend fun <T> dbQuery(statement: Transaction.() -> T): T =
    withContext(Dispatchers.IO) {
        transaction(DbSettings.connection, statement)
    }

object UrlMappingTable : IntIdTable("url_mapping") {
    val from: Column<String> = varchar("from", MODEL_URL_LENGTH).uniqueIndex()
    val to: Column<String> = varchar("to", 2048).uniqueIndex()
}

suspend fun getShortenedLink(call: ApplicationCall, toBeShortened: String): String {
    val shortenId = searchForSavedShortening(toBeShortened) ?: run {
        val generatedId = randomString()
        insertShortening(toBeShortened, generatedId)
        generatedId
    }
    return generateShortenedLink(call, shortenId)
}

suspend fun getMappedLocation(shortenId: String): String? = dbQuery {
    UrlMappingTable.select {
        UrlMappingTable.from eq shortenId
    }.map { it[UrlMappingTable.to] }.firstOrNull()
}

private object DbSettings {
    val connection by lazy {
        Database.connect(JDBC_DATABASE_URL, driver = "org.postgresql.Driver")
    }
}

private fun generateShortenedLink(call: ApplicationCall, shortenId: String)
        = "${if (IS_DEV) "http" else "https"}://${call.request.host()}${if (IS_DEV) ":" + call.request.port() else ""}/$shortenId"

private suspend fun searchForSavedShortening(toBeShortened: String): String? = dbQuery {
    UrlMappingTable.select {
        UrlMappingTable.to eq toBeShortened
    }.map { it[UrlMappingTable.from] }.firstOrNull()
}

private suspend fun insertShortening(toBeShortened: String, generatedId: String) = dbQuery {
    UrlMappingTable.insert {
        it[from] = generatedId
        it[to] = toBeShortened
    }
}
